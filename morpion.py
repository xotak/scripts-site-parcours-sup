grille = [[" ", " ", " "],
          [" ", " ", " "],
          [" ", " ", " "]]


def dessine(grille:list) -> None:
    """
    Dessine la grille dans la console
    """
    print( "     a     b     c   ")
    print( "   _____ _____ _____ ")
    print( "  |     |     |     |")
    print(f"1 |  {grille[0][0]}  |  {grille[0][1]}  |  {grille[0][2]}  |")
    print( "  |_____|_____|_____|")
    print( "  |     |     |     |")
    print(f"2 |  {grille[1][0]}  |  {grille[1][1]}  |  {grille[1][2]}  |")
    print( "  |_____|_____|_____|")
    print( "  |     |     |     |")
    print(f"3 |  {grille[2][0]}  |  {grille[2][1]}  |  {grille[2][2]}  |")
    print( "  |_____|_____|_____|")

def coup_valide(grille, coup):
    """
    Vérifie si le coup est valide
    Teste si le coup est bien formé, puis que la cas est vide
    """
    
    ligne_coup = coup[1]
    colonne_coup = coup[0]
    
    if len(coup) != 2:
        return False
    
    if colonne_coup == "a":
        colonne_coup = 0
    elif colonne_coup == "b":
        colonne_coup = 1
    elif colonne_coup == "c":
        colonne_coup = 2
    else:
        return False
    
    if ligne_coup == "1":
        ligne_coup = 0
    elif ligne_coup == "2":
        ligne_coup = 1
    elif ligne_coup == "3":
        ligne_coup = 2
    else:
        return False

    if grille[colonne_coup][ligne_coup] == " ":
        return True


def joue_coup(grille,coup,jeton):
    """
    Place le jeton proposé à la position indiquée par le coup dans la grille
    Renvoie la grille
    """
    grille["123".index(coup[1])]["abc".index(coup[0])] = jeton
    
    
def victoire(grille):
    """
    Indique si la grille est victorieuse pour ce jeton
    """
    if grille[0][0]==grille[0][1]==grille[0][2] != " " or grille[1][0]==grille[1][1]==grille[1][2] != " " or grille[2][0]==grille[2][1]==grille[2][2] != " " : return True
    elif grille[0][0]==grille[1][0]==grille[2][0] != " " or grille[0][1]==grille[1][1]==grille[2][1] != " " or grille[0][2]==grille[1][2]==grille[2][2] != " " : return True
    elif grille[0][0]==grille[1][1]==grille[2][2] != " " or grille[0][2]==grille[1][1]==grille[2][0] != " " : return True
    else : return False


nb_tours = 0
victorieux = False

while nb_tours != 9 and victorieux != True:
    dessine(grille)
    nb_tours += 1
    if nb_tours % 2 == 1:    
        coup = input("Saisir le coup de X : ")
        if coup_valide(grille, coup) == True:    
            joue_coup(grille, coup, "X")
            victorieux = victoire(grille)
            if victorieux == True:
                dessine(grille)
                print("X à gagné")
        else:
            print("Coup invalide, veuillez réessayer")
            nb_tours -= 1
    else:
        coup = input("Saisir le coup de 0 : ")
        if coup_valide(grille, coup) == True: 
            joue_coup(grille, coup, "0")
            victorieux = victoire(grille)
            if victorieux == True:
                dessine(grille)
                print("0 à gagné")
        else:
            print("Coup invalide, veuillez réessayer")
            nb_tours -= 1   