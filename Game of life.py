import os
from time import sleep

LARGEUR = 30
largeur = LARGEUR-1
MORT = "."
VIVANT = "@"

def grille_vide():
    return [ [MORT for _ in range(LARGEUR)] for __ in range(LARGEUR)]

def grille_en_chaine(grille):
    lignes = [ " ".join(ligne) for ligne in grille]
    return "\n".join(lignes)


grille = grille_vide()
i=5
j=5
grille[i][j] = VIVANT
#grille[i-1][j] = VIVANT
grille[i-1][j-1] = VIVANT
grille[i][j-1] = VIVANT
#grille[i][j+1] = VIVANT
grille[i+1][j+1] = VIVANT
#grille[i+1][j] = VIVANT
grille[i+1][j-1] = VIVANT

grille[i-1][j+1] = VIVANT
chaine = grille_en_chaine(grille)
print(chaine)

def nbr_voisins(grille, i, j):
    nb=0
    if i!=0 :
        if grille[i-1][j]   == VIVANT: nb+=1
    if i!=0 and j!=0:
        if grille[i-1][j-1] == VIVANT: nb+=1
    if j!=0:
        if grille[i][j-1] == VIVANT: nb+=1
    if j!=largeur:
        if grille[i][j+1] == VIVANT: nb+=1
    if i!=largeur and j!=largeur:
        if grille[i+1][j+1] == VIVANT: nb+=1
    if i!= largeur:
        if grille[i+1][j] == VIVANT: nb+=1
    if i!= largeur and j!=0:
        if grille[i+1][j-1] == VIVANT : nb+=1
    if i!=0 and j !=largeur:
        if grille[i-1][j+1] == VIVANT : nb+=1
    return nb
    

    
while True:
    chaine = grille_en_chaine(grille)
    print(chaine)
    step = grille_vide()
    for i in range(0, LARGEUR):
        for j in range(0, LARGEUR):
            nVoisins = nbr_voisins(grille,i,j)
            if grille[i][j] == VIVANT :
                if nVoisins <= 1 or nVoisins >= 4: step[i][j]=MORT
                else : step[i][j] = VIVANT
            else : 
                if nVoisins == 3 : step[i][j]=VIVANT
    if grille == step: break
    grille = step
    os.system("cls")
    print(grille_en_chaine(grille))
    sleep(0.5)
    

print("ENDED")